package com.pack;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewTest1 {
  @Test(dependsOnMethods="newCustomer")
  public void modifyCustomer() {
	  System.out.println("The Customer will Be Modified ");
	  
  }
  @Test(dependsOnMethods="modifyCustomer")
  public void createCustomer() {
	  System.out.println("The Customer will Be Created");
	  
  }
  @Test(priority = 2)
  public void newCustomer() {
	  System.out.println("The Customer will Be Created");
	  
  }
  
  @BeforeMethod
  public void beforeCustomer() {
	  System.out.println("Verify the Customer");
	  
  }
  @AfterMethod
  public void afterCustomer() {
	  System.out.println("All Transcation are Done");
	  
  }
  @BeforeClass
  public void beforeclass() {
	  System.out.println("Start data base connection,launch browser");
	  
  }
  @AfterClass
  
  public void afterclass() {
	  System.out.println("Close data base connection,close browser");
	  
  }
 
  
}

package com.pack;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Assignment1 {
  @Test
	public void launchApplication() {
		WebDriver driver=new ChromeDriver();
		driver.get("https://demo.actitime.com/login.do");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		String text="Please identify yourself";
		if(driver.getPageSource().contains("Please identify yourself")) 
		{
			System.out.println("The expected text is present  "+text);

		}
		else 
		{
			System.out.println("The expected text is not present  "+text);

		}
		WebElement logoPresent = driver.findElement(By.xpath("//div[@class='atLogoImg']"));
      if(logoPresent.isDisplayed()) 
      {
			System.out.println("The logo is present");
	
      }
      else
      {
			System.out.println("The logo is not present");

      }
	}
  
}


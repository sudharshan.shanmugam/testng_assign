package com.pack;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NewTest3 {
	 @Test(groups={"Smoke Test"})
	  public void createCustomer() {
		  System.out.println("Creating customer");
	  }
	  @Test(groups={"Regression Test"})
	  public void modifyCustomer() {
		  System.out.println("the customer has modified");
	  }
	  @Test(groups={"ReTest"})
	  public void newCustomer() {
		  System.out.println("Creating the new customer");
	  }
	  @Test(groups={"Smoke Test"})
	  public void changeCustomer() {
		  System.out.println("Changing the customer");
	  }
	  @BeforeClass
	  public void beforeclass() {
		  System.out.println("cheking preconditions");
	  }
	  @AfterClass
	  public void afterclass() {
		  System.out.println("cheked postconditions");
	  }
	  
}
